![](https://raw.githubusercontent.com/rwth-acis/las2peer/master/img/logo/bitmap/las2peer-logo-128x128.png)

# las2peer-Eye-Focus-Task-Tracker

The las2peer-Eye-Focus-Task-Tracker is a service to facilitate communication between the Eye-Focus-Taks-Tracker and a designated Learning Record Store.
* The Eye-Focus-Task-Tracker ([project](https://gitlab.com/johannesctrl/eye-focus-task-tracker)) is a Python-Script which is reading the gaze and video of eye tracking glasses. By using the ONYX-Interaction-Tracker ([project](https://gitlab.com/johannesctrl/onyx-interactions-tracker)) which creates QR-Codes and ArUco-Marker into an ONYX Testsuite working sheet, the program is able to detect the position and area of tasks and task elements to compare it with the gaze information to know if a person is looking at a task or a task element.
* The Learning Record Store is a storage to store learning achievements of a person or a group. The used kind of LRS for this project is the [Learning Locker](https://docs.learninglocker.net/welcome/).

As the following image shows this service converts a json formatted user interaction created by the ONYX-User-Interaction-Tracker into an xAPI statement. This xAPI statement can be accepted by the Learning Record Store.

![](show.jpg)

The project is based on the [las2peer-template-project](https://github.com/rwth-acis/las2peer-template-project)
For documentation on the las2peer service API, please refer to the [wiki](https://github.com/rwth-acis/las2peer-Template-Project/wiki).


## REST API calls
The service provides the two following api calls.

|request method|url suffix|description|
|---|---|---|
|GET|/las2peer-eye-focus-task-tracker/get-check-connection|This method simply checks if the service can be reached by sending back a string with value "connected".|
|POST|/las2peer-eye-focus-task-tracker/post-eye-tracking|This method needs a json object created by the Eye-Focus-Task-Tracker as the POST-payload to send it via MobSOS to the Learning Record Store. It converts the json object into a xAPI statement which is needed by the LRS.|
|POST|/las2peer-eye-focus-task-tracker/post-raw-data|This method needs a json object which holds raw data created a project through the network api of the pupil core, creates an xAPI statement out of it to send it via MobSOS to the LRS.  This method is less recommended than the array method because the network api creates a lot of information in a second which shouldn't be sent individually at this service.|
|POST|/las2peer-eye-focus-task-tracker/post-raw-data-array|This method needs a json object which holds raw data as an array created by a project through the network api of the pupil core, creates an xAPI statement out of it to send it via MobSOS to the LRS.|
