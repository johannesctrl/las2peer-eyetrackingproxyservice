package i5.las2peer.services.eyeTrackingProxyService;

import org.json.JSONException;
import org.json.JSONObject;
import i5.las2peer.logging.L2pLogger;
// import com.rusticisoftware.tincan.*;


public class StatementGeneratorTaskTracker {

    private final String TECH4COMP_URI = "https://tech4comp.de/xapi";
    private final L2pLogger log = L2pLogger.getInstance(StatementGeneratorTaskTracker.class.getName());

    public JSONObject createStatementFromAppData(JSONObject dataJSON, L2pLogger logger) {
        logger.info("createStatementFromAppData-Method is called");

        String userId = dataJSON.getString("userId");
        String studyName = dataJSON.getString("studyName");

        JSONObject retStatement = new JSONObject();

        // Add actor
        try {
            JSONObject actorJSON =  createActor(userId);
            retStatement.put("actor", actorJSON);
        } catch (JSONException e) {
            log.severe("There was a problem parsing the actor data");
            return null;
        }

        //Add verb
        try {
            JSONObject verbJSON = createVerb("evaluated");
            retStatement.put("verb", verbJSON);
        } catch (JSONException e) {
            log.severe("There was a problem parsing the verb data");
            return null;
        }

        //Add object
        try {
            JSONObject objectJSON = createObject(studyName);
            retStatement.put("object", objectJSON);
        } catch (JSONException e) {
            log.severe("There was a problem parsing the object data");
            return null;
        }

        //Add context extensions
        try {
            JSONObject contextJSON = createContextExtensions(dataJSON, logger);
            retStatement.put("context", contextJSON);
        } catch (JSONException e) {
            log.severe("There was a problem parsing the context extension data");
            return null;
        }

        return retStatement;
    }

    private JSONObject createActor(String userId) {
        JSONObject actorJSON = new JSONObject();
        actorJSON.put("objectType", "Agent");
        String mboxValue = "mailto:" + userId + "@tech4comp.com";
        actorJSON.put("mbox", mboxValue);

        return actorJSON;
    }

    private JSONObject createVerb(String verb) {
        JSONObject verbJSON = new JSONObject();

        String id = TECH4COMP_URI + "/verbs/" + verb;
        verbJSON.put("id", id);

        JSONObject displayJSON = new JSONObject();
        displayJSON.put("en-US", verb);
        verbJSON.put("display", displayJSON);

        return verbJSON;
    }

    private JSONObject createObject(String studyName) throws JSONException {
        JSONObject objectJSON = new JSONObject();

        String id = TECH4COMP_URI + "/activities/study_" + studyName;
        objectJSON.put("id", id);

        String objectType = "Activity";
        objectJSON.put("objectType", objectType);

        JSONObject definitionJSON = new JSONObject();
        JSONObject nameJSON = new JSONObject();
        nameJSON.put("en-US", "Study_" + studyName);
        JSONObject descriptionJSON = new JSONObject();
        descriptionJSON.put("en-US", "A tech4comp Eye-Tracking study evaluation");
        definitionJSON.put("name", nameJSON);
        definitionJSON.put("description", descriptionJSON);
        objectJSON.put("definition", definitionJSON);

        return objectJSON;
    }

    private JSONObject createContextExtensions(JSONObject dataJSON, L2pLogger logger) throws JSONException {
        logger.info("createContextExtensions-Method is called");

        String taskId = dataJSON.getString("taskId");
        logger.info("A1 successfull");
        String taskElementId = dataJSON.getString("taskElementId");
        logger.info("A2 successfull");

        String timestamp = dataJSON.getString("timestamp");
        logger.info("B1 successfull");
        String startTimestamp = dataJSON.getString("startTimestamp");
        logger.info("B2 successfull");

        String iri = TECH4COMP_URI + "/context/extensions/";
        JSONObject extensionsJSON = new JSONObject();
        logger.info("C successfull");

        extensionsJSON.put(iri + "taskId", taskId);
        extensionsJSON.put(iri + "taskElementId", taskElementId);
        logger.info("D successfull");

        extensionsJSON.put(iri + "timestamp", timestamp);
        extensionsJSON.put(iri + "startTimestamp", startTimestamp);
        logger.info("E successfull");

        JSONObject contextJSON = new JSONObject();
        contextJSON.put("extensions", extensionsJSON);
        logger.info("F successfull");
        return contextJSON;
    }
}