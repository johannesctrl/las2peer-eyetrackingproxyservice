package i5.las2peer.services.eyeTrackingProxyService;

import i5.las2peer.logging.L2pLogger;
import org.json.JSONException;
import org.json.JSONObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import i5.las2peer.logging.L2pLogger;
import i5.las2peer.api.Context;
import java.math.BigInteger;
import java.net.URISyntaxException;

public class StatementGeneratorPostRawData {

    private final String TECH4COMP_URI = "https://tech4comp.de/xapi";
    private final L2pLogger log = L2pLogger.getInstance(StatementGeneratorPostRawData.class.getName());

    public JSONObject createStatementFromAppData(JSONObject dataJSON, L2pLogger logger, int statementIndex) {
        logger.info("createStatementFromAppData-Method is called");

        String userId = dataJSON.getString("userId");
        String studyName = dataJSON.getString("studyName");

        JSONObject retStatement = new JSONObject();

        // Add actor
        try {
            JSONObject actorJSON =  createActor(userId);
            retStatement.put("actor", actorJSON);
        } catch (JSONException e) {
            log.severe("There was a problem parsing the actor data");
            return null;
        }

        //Add verb
        try {
            JSONObject verbJSON = createVerb("evaluated");
            retStatement.put("verb", verbJSON);
        } catch (JSONException e) {
            log.severe("There was a problem parsing the verb data");
            return null;
        }

        //Add object
        try {
            JSONObject objectJSON = createObject(studyName);
            retStatement.put("object", objectJSON);
        } catch (JSONException e) {
            log.severe("There was a problem parsing the object data");
            return null;
        }

        //Add context extensions
        try {
            if (statementIndex == 0) {
                JSONObject contextJSON = createContextExtensions_ForSingle(dataJSON, logger);
                retStatement.put("context", contextJSON);
            }
            if (statementIndex == 1) {
                JSONObject contextJSON = createContextExtensions_ForArray(dataJSON, logger);
                retStatement.put("context", contextJSON);
            }
        } catch (JSONException e) {
            log.severe("There was a problem parsing the context extension data");
            return null;
        }

        return retStatement;
    }

    private JSONObject createActor(String userId) {
        JSONObject actorJSON = new JSONObject();
        actorJSON.put("objectType", "Agent");
        String mboxValue = "mailto:" + userId + "@tech4comp.com";
        actorJSON.put("mbox", mboxValue);

        return actorJSON;
    }

    private JSONObject createVerb(String verb) {
        JSONObject verbJSON = new JSONObject();

        String id = TECH4COMP_URI + "/verbs/" + verb;
        verbJSON.put("id", id);

        JSONObject displayJSON = new JSONObject();
        displayJSON.put("en-US", verb);
        verbJSON.put("display", displayJSON);

        return verbJSON;
    }

    private JSONObject createObject(String studyName) throws JSONException {
        JSONObject objectJSON = new JSONObject();

        String id = TECH4COMP_URI + "/activities/study_" + studyName;
        objectJSON.put("id", id);

        String objectType = "Activity";
        objectJSON.put("objectType", objectType);

        JSONObject definitionJSON = new JSONObject();
        JSONObject nameJSON = new JSONObject();
        nameJSON.put("en-US", "Study_" + studyName);
        JSONObject descriptionJSON = new JSONObject();
        descriptionJSON.put("en-US", "A tech4comp Eye-Tracking study evaluation");
        definitionJSON.put("name", nameJSON);
        definitionJSON.put("description", descriptionJSON);
        objectJSON.put("definition", definitionJSON);

        return objectJSON;
    }

    private JSONObject createContextExtensions_ForSingle(JSONObject dataJSON, L2pLogger logger) throws JSONException {
        logger.info("createContextExtensions-Method is called");

        String left_diameter = dataJSON.getString("left_diameter");
        logger.info("left_diameter successful");
        String right_diameter = dataJSON.getString("right_diameter");
        logger.info("right_diameter successful");

        String system_time = dataJSON.getString("system_time");
        logger.info("system_time successful");
        String pupil_timestamp = dataJSON.getString("pupil_timestamp");
        logger.info("pupil_timestamp successful");

        String norm_pos_x = dataJSON.getString("norm_pos_x");
        logger.info("norm_pos_x successful");
        String norm_pos_y = dataJSON.getString("norm_pos_y");
        logger.info("norm_pos_y successful");

        String iri = TECH4COMP_URI + "/context/extensions/eyeTrackingRawData";
        JSONObject extensionsJSON = new JSONObject();
        logger.info("extensionsJSON successful");

        JSONObject eyeTrackingRawData_singleEntry = new JSONObject();
        eyeTrackingRawData_singleEntry.put("left_diameter", left_diameter);
        eyeTrackingRawData_singleEntry.put("right_diameter", right_diameter);
        eyeTrackingRawData_singleEntry.put("system_time", system_time);
        eyeTrackingRawData_singleEntry.put("pupil_timestamp", pupil_timestamp);
        eyeTrackingRawData_singleEntry.put("norm_pos_x", norm_pos_x);
        eyeTrackingRawData_singleEntry.put("norm_pos_y", norm_pos_y);
        extensionsJSON.put(iri, eyeTrackingRawData_singleEntry);
        logger.info("eyeTrackingRawData_singleEntry successful");

        JSONObject contextJSON = new JSONObject();
        contextJSON.put("extensions", extensionsJSON);
        logger.info("context successful");
        return contextJSON;
    }

    private JSONObject createContextExtensions_ForArray(JSONObject dataJSON, L2pLogger logger) throws JSONException {
        logger.info("createContextExtensions-Method is called");

        String iri = TECH4COMP_URI + "/context/extensions/eyeTrackingRawData";
        JSONObject extensionsJSON = new JSONObject();
        logger.info("extensionsJSON successful");

        JSONArray eyeTrackingRawData_allEntries = new JSONArray();
        JSONArray records = dataJSON.getJSONArray("records");
        for (int i = 0; i < records.length(); i++) {

            // records attributes to variables
            JSONObject record = records.getJSONObject(i);
            String left_diameter = record.getString("left_diameter");
            logger.info("left_diameter successful");
            String right_diameter = record.getString("right_diameter");
            logger.info("right_diameter successful");
            String system_time = record.getString("system_time");
            logger.info("system_time successful");
            String pupil_timestamp = record.getString("pupil_timestamp");
            logger.info("pupil_timestamp successful");
            String norm_pos_x = record.getString("norm_pos_x");
            logger.info("norm_pos_x successful");
            String norm_pos_y = record.getString("norm_pos_y");
            logger.info("norm_pos_y successful");

            // write record attributes to single entry
            JSONObject eyeTrackingRawData_singleEntry = new JSONObject();
            eyeTrackingRawData_singleEntry.put("left_diameter", left_diameter);
            eyeTrackingRawData_singleEntry.put("right_diameter", right_diameter);
            eyeTrackingRawData_singleEntry.put("system_time", system_time);
            eyeTrackingRawData_singleEntry.put("pupil_timestamp", pupil_timestamp);
            eyeTrackingRawData_singleEntry.put("norm_pos_x", norm_pos_x);
            eyeTrackingRawData_singleEntry.put("norm_pos_y", norm_pos_y);

            eyeTrackingRawData_allEntries.put(eyeTrackingRawData_singleEntry);
            logger.info("eyeTrackingRawData_singleEntry successful");
        }

        extensionsJSON.put(iri, eyeTrackingRawData_allEntries);

        JSONObject contextJSON = new JSONObject();
        contextJSON.put("extensions", extensionsJSON);
        logger.info("context successful");
        return contextJSON;
    }

}